package main.java.no.experis.hero;

public enum HeroClass {
    WARRIOR,
    RANGER,
    MAGE
}
