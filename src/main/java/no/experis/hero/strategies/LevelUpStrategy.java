package main.java.no.experis.hero.strategies;

import main.java.no.experis.hero.HeroStats;

public interface LevelUpStrategy {
    void levelUp(HeroStats heroStats);
}
