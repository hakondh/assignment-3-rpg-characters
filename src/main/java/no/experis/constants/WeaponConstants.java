package main.java.no.experis.constants;

public class WeaponConstants {
    // Scaling for different weapon slots (only one now, but more could be added)
    public static final int WEAPON_SCALE = 100;

    // Base stats for different types of weapons
    public static final int MELEE_BASE_DMG = 15;
    public static final int RANGED_BASE_DMG = 5;
    public static final int MAGIC_BASE_DMG = 25;
}
