package main.java.no.experis.constants;

public class MageConstants {
    // All the starting values for a mage
    public static final int START_HP = 100;
    public static final int START_STR = 2;
    public static final int START_DEX = 3;
    public static final int START_INT = 10;

    // All the leveling values for a mage
    public static final int LEVEL_UP_HP_GAIN = 15;
    public static final int LEVEL_UP_STR_GAIN = 1;
    public static final int LEVEL_UP_DEX_GAIN = 5;
    public static final int LEVEL_UP_INT_GAIN = 5;
}
