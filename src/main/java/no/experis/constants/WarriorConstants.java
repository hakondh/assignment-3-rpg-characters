package main.java.no.experis.constants;

public class WarriorConstants {
    // All the starting values for a warrior
    public static final int START_HP = 150;
    public static final int START_STR = 10;
    public static final int START_DEX = 3;
    public static final int START_INT = 1;

    // All the leveling values for a warrior
    public static final int LEVEL_UP_HP_GAIN = 30;
    public static final int LEVEL_UP_STR_GAIN = 5;
    public static final int LEVEL_UP_DEX_GAIN = 2;
    public static final int LEVEL_UP_INT_GAIN = 1;
}
