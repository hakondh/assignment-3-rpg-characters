package main.java.no.experis.constants;

public class ArmorConstants {
    // Scaling for different armor slots
    public static final double BODY_SCALE = 1.0;
    public static final double HEAD_SCALE = 0.8;
    public static final double LEGS_SCALE = 0.6;

    // Base stats for different types of armor (HP, STR, DEX, INT)
    // CLOTH
    public static final int CLOTH_HP_BASE = 10;
    public static final int CLOTH_STR_BASE = 0;
    public static final int CLOTH_INT_BASE = 3;
    public static final int CLOTH_DEX_BASE = 1;

    // LEATHER
    public static final int LEATHER_HP_BASE = 20;
    public static final int LEATHER_STR_BASE = 1;
    public static final int LEATHER_INT_BASE = 0;
    public static final int LEATHER_DEX_BASE = 3;

    // PLATE
    public static final int PLATE_HP_BASE = 30;
    public static final int PLATE_STR_BASE = 3;
    public static final int PLATE_INT_BASE = 0;
    public static final int PLATE_DEX_BASE = 1;

    // Bonus stats for different types of armor (HP, STR, DEX, INT)
    // CLOTH
    public static final int CLOTH_HP_BONUS = 5;
    public static final int CLOTH_STR_BONUS = 0;
    public static final int CLOTH_INT_BONUS = 2;
    public static final int CLOTH_DEX_BONUS = 1;

    // LEATHER
    public static final int LEATHER_HP_BONUS = 8;
    public static final int LEATHER_STR_BONUS = 1;
    public static final int LEATHER_INT_BONUS = 0;
    public static final int LEATHER_DEX_BONUS = 2;

    // PLATE
    public static final int PLATE_HP_BONUS = 12;
    public static final int PLATE_STR_BONUS = 2;
    public static final int PLATE_INT_BONUS = 0;
    public static final int PLATE_DEX_BONUS = 1;
}
