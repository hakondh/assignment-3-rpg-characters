package main.java.no.experis.constants;

public class RangerConstants {
    // All the starting values for a ranger
    public static final int START_HP = 120;
    public static final int START_STR = 5;
    public static final int START_DEX = 10;
    public static final int START_INT = 2;

    // All the leveling values for a ranger
    public static final int LEVEL_UP_HP_GAIN = 20;
    public static final int LEVEL_UP_STR_GAIN = 2;
    public static final int LEVEL_UP_DEX_GAIN = 5;
    public static final int LEVEL_UP_INT_GAIN = 1;
}
